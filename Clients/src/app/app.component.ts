import { Component } from '@angular/core';
import { SignalRService } from './services/signal-r.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(public signalRService: SignalRService, private http: HttpClient) { }

  ngOnInit() {
    setTimeout(() => this.signalRCall(), 10000);
  }



  signalRCall() {
    this.signalRService.startConnection();
    this.signalRService.addAutoUpdateTransferListener();
    this.signalRService.addRenderingTransferListener();
    this.signalRService.addRenderProcessListener();
    this.signalRService.addRestartConnectionEvent();
    this.signalRService.startHttpRequest();
  }


}
