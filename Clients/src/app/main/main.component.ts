import { ConfigModel } from './../models/configModel';
import { StoryInputModel } from './../models/inputModels/storyInputModel';
import { ConfigService } from './../services/config.service';
import { SignalRService } from './../services/signal-r.service';
import { StoryModel } from './../models/storyModel';
import { WebModel } from './../models/webModel';
import { WebService } from './../services/web.service';
import { ChapterModel } from './../models/chapterModel';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {


  constructor(
    public webService: WebService,
    public signalRService: SignalRService,
    public configService: ConfigService
  ) { }

  ngOnInit() {
    this.loadConfig();
    this.webService.getWebs();
  }


  setSelectedStory(story: StoryModel) {
    this.webService.selectedStory = story;
    this.webService.getChapters(this.webService.selectedStory.id);
  }

  setSelectedWeb(web: WebModel) {
    this.webService.selectedWeb = web;
    this.webService.showStories(this.webService.selectedWeb.id);
  }




  postStory() {
    let storyInputObject = new StoryInputModel;
    storyInputObject.url = this.webService.newStoryLink;
    storyInputObject.webId = this.webService.selectedWeb.id;
    this.webService.addStory(storyInputObject).subscribe(data => {
      this.webService.getWebs();
      console.log(data);
    });
    this.webService.newStoryLink = "";
  }


  render(chapterId: number) {
    this.webService.renderChapter(chapterId).subscribe(res => console.log(res));
  }

  loadConfig() {
    this.webService.configSaveLoading = true;
    this.configService.getConfig().subscribe(res => {
      this.webService.config = res as ConfigModel;
      if (this.webService.config != null) {
        this.webService.configSaveLoading = false;
      }
    });
  }

  saveConfig() {
    this.webService.configSaveLoading = true;
    return this.configService.save(this.webService.config).subscribe(res => {
      this.webService.config = res as ConfigModel
      this.webService.configSaveLoading = false;
    });
  }

  toggleAutoRender(story: StoryModel) {
    this.webService.toggleAutoRender(story.id).subscribe(data => {
      this.webService.getWebs();
    });
  }

  deleteStory(storyId: number) {
    this.webService.deleteStory(storyId).subscribe(res => {
      console.log("Delete story response: ", res);
      this.webService.getWebs();
    });
  }

  //Update chapters in each story
  update() {
    this.webService.update().subscribe(res => {
      this.webService.updateStatus = res;
      console.log(res);

      //Get webs
      this.webService.getWebs();
    });
  }




}
