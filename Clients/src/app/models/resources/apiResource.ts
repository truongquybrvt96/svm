export class APIResource {
    success: boolean;
    dateTime: Date;
    data: object;
    error: object;
}