import { ChapterModel } from './../chapterModel';
export class RenderProcessResource {
    rendered: ChapterModel[];
    comings: ChapterModel[];
    rendering: ChapterModel;
    fails: ChapterModel[];
}