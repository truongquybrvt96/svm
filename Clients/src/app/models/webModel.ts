import { StoryModel } from './storyModel';
export class WebModel {
    id: number;
    name: string;
    domain: string;
    stories: StoryModel[];
}