export interface ConfigModel {
    id: number,
    name: string,
    saveFolder: string;
    newChapterCheck: number;
}