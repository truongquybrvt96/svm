import { ChapterModel } from './chapterModel';
export class StoryModel {
    id: number;
    name: string;
    url: string;
    chapters: ChapterModel[];
    autoRender: boolean;
    webId: number;
}