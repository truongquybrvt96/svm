export class ChapterModel {
    id: number;
    name: string;
    url: string;
    newlyAdded: boolean;
    storyId: number
}