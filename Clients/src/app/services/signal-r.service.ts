import { APIResource } from './../models/resources/apiResource';
import { MainComponent } from './../main/main.component';
import { WebService } from './web.service';
import { ChapterModel } from './../models/chapterModel';
import { Injectable } from '@angular/core';
import * as signalR from "@aspnet/signalr";
import { ChartModel } from '../interfaces/chartModel';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { retry } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
};

@Injectable({
  providedIn: 'root'
})
export class SignalRService {
  private baseUrl = "http://localhost:7778";
  public updateStory: boolean = false;
  public foundNewChapters: number;
  public bradcastedData: ChartModel[];
  public renderProcess: APIResource;

  private hubConnection: signalR.HubConnection

  /**
   *
   */
  constructor(
    private webService: WebService,
    private http: HttpClient
  ) {
  }

  public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(this.baseUrl + '/autocheck')
      .build();

    this.hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ' + err))
  }
  //Update chapters in stories
  public addAutoUpdateTransferListener = () => {
    this.hubConnection.on('UpdateStory', (data) => {
      this.updateStory = data;
      console.log("UpdateStory: ", data);
      if (data && this.webService.selectedWeb) {
        this.webService.getChapters(this.webService.selectedStory.id);
      }
    });
  }

  //New chapters found: boolean
  public addRenderingTransferListener = () => {
    this.hubConnection.on('FindNewChapters', (data) => {
      this.foundNewChapters = data;
      console.log("FindNewChapters: " + data as string);
    });
  }

  //Render process
  public addRenderProcessListener = () => {
    this.hubConnection.on('RenderProcess', (data) => {
      //this.renderProcess = data.data;
      console.log("RenderProcess triggered.");
      console.log(data);
    });
  }


  public startHttpRequest = () => {
    this.http.get(this.baseUrl + '/api/autocheck', httpOptions)
      .subscribe(res => {
        console.log(res);
      }),
      retry(5)
  }

  public addRestartConnectionEvent = () => {
    this.hubConnection.onclose(() => {
      setTimeout(() => {
        this.hubConnection.start();
      }, 5000)
      console.log("Connection restarting in 5 seconds...");
    });
  }

}
