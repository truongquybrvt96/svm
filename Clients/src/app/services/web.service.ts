import { APIResource } from './../models/resources/apiResource';
import { StoryInputModel } from './../models/inputModels/storyInputModel';
import { WebModel } from './../models/webModel';
import { ConfigModel } from './../models/configModel';
import { ChapterModel } from './../models/chapterModel';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StoryModel } from '../models/storyModel';

@Injectable({ providedIn: 'root' })
export class WebService {
    private baseUrl = "http://localhost:7778";
    private getWebsUrl = this.baseUrl + "/api/webs/getwebs";
    private getChaptersUrl = this.baseUrl + "/api/webs/getChapters";
    private renderChapterUrl = this.baseUrl + "/api/webs/render";
    private getConfigUrl = this.baseUrl + "/api/webs/getconfig";
    private postStoryUrl = this.baseUrl + "/api/webs/createStory";
    private deleteStoryUrl = this.baseUrl + "/api/webs/deleteStory";
    private toggleAutoRenderUrl = this.baseUrl + "/api/webs/ToggleAutoRender";
    private updateUrl = this.baseUrl + "/api/webs/update";

    webs: WebModel[];
    selectedStory: StoryModel;
    chapters: ChapterModel[];
    selectedWeb: WebModel;
    stories: StoryModel[];
    showUpdateLoading: boolean = false;
    configSaveLoading: boolean = false;
    config: ConfigModel;
    newStoryLink: string;
    updateStatus: APIResource;

    constructor(
        private httpClient: HttpClient
    ) { }

    getWebs() {
        this.httpClient.get<WebModel[]>(this.getWebsUrl).subscribe((data) => {
            //Get webs
            this.webs = data;

            //Show stories
            if (this.selectedWeb != null)
                this.showStories(this.selectedWeb.id);

            //Load and show chapters
            if (this.selectedStory != null)
                this.getChapters(this.selectedStory.id);
        });
    }

    //Set stories to model
    showStories(webId: number) {
        if (webId != null)
            this.stories = this.webs.find(w => w.id == webId).stories;
    }

    //Get and set chapters to model
    getChapters(storyId: number) {
        if (storyId == null) {

        }
        return this.httpClient.get<ChapterModel[]>(this.getChaptersUrl + "/" + storyId).subscribe(chapters => {
            this.chapters = chapters;
        });
    }

    //Post story url to server
    addStory(storyInput: StoryInputModel) {
        return this.httpClient.post<StoryInputModel>(this.postStoryUrl, storyInput);
    }

    //Post a chapter to server to render
    renderChapter(chapterId: number) {
        return this.httpClient.get<number>(this.renderChapterUrl + "/" + chapterId);
    }

    //Toggle AutoRender property of a story
    toggleAutoRender(storyId: number) {
        return this.httpClient.get(this.toggleAutoRenderUrl + "/" + storyId);
    }

    deleteStory(storyId: number) {
        return this.httpClient.delete(this.deleteStoryUrl + "/" + storyId);
    }

    update() {
        return this.httpClient.get<APIResource>(this.updateUrl);
    }





}