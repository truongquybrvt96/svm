import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ConfigModel } from './../models/configModel';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private baseUrl = "http://localhost:7778";
  private setDefaultUrl = this.baseUrl + "/api/config/setdefault";
  private getUrl = this.baseUrl + "/api/config/get";
  private setUrl = this.baseUrl + "/api/config/save";

  constructor(private httpClient: HttpClient) { }

  getConfig() {
    return this.httpClient.get<ConfigModel>(this.getUrl);
  }

  save(model: ConfigModel): Observable<ConfigModel> {
    return this.httpClient.post<ConfigModel>(this.setUrl, model);
  }

}
