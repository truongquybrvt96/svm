﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SVM.Extensions
{
    public static class RemoveSplashtnr
    {
        public static string RemoveTrashChar(this string input)
        {
            return input.Replace("\n", String.Empty)
                .Replace("\r", String.Empty)
                .Replace("\t", String.Empty)
                .Replace("&nbsp;", String.Empty);
        }
    }
}
