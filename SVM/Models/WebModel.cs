﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SVM.Models
{
    [Table("Webs")]
    public class WebModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Domain { get; set; }

        public ICollection<StoryModel> Stories { get; set; }

        public WebModel()
        {
            Stories = new List<StoryModel>();
        }
    }
}
