﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SVM.Models
{
    [Table("Chapters")]
    public class ChapterModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Url { get; set; }
        public bool NewlyAdded { get; set; }

        public int StoryId { get; set; }
        public StoryModel Story { get; set; }


        public ChapterModel()
        {
            Name = String.Empty;
            Url = String.Empty;
            NewlyAdded = false;
        }
    }
}
