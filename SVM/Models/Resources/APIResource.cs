﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SVM.Models.Resources
{
    public class APIResource
    {
        public bool Success { get; set; }
        public DateTime DateTime { get; set; }
        public object Data { get; set; }
        public object Error { get; set; }
    }
}
