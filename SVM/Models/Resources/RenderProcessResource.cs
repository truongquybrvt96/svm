﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SVM.Models.Resources
{
    public class RenderProcessResource
    {
        public List<ChapterModel> Rendered { get; set; }
        public List<ChapterModel> Comings { get; set; }
        public ChapterModel Rendering { get; set; }
        public List<ChapterModel> Fails { get; set; }

        public RenderProcessResource()
        {
            Rendered = new List<ChapterModel>();
            Comings = new List<ChapterModel>();
            Fails = new List<ChapterModel>();
        }
    }
}
