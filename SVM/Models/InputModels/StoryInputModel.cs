﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SVM.Models.InputModels
{
    public class StoryInputModel
    {
        [Required]
        public string Url { get; set; }
        [Required]
        public int WebId { get; set; }
    }
}
