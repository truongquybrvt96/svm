﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SVM.Models
{
    [Table("Stories")]
    public class StoryModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Url { get; set; }
        public bool AutoRender { get; set; }

        public int WebId { get; set; }
        public WebModel Web { get; set; }


        public ICollection<ChapterModel> Chapters { get; set; }

        public StoryModel()
        {
            Chapters = new List<ChapterModel>();
            AutoRender = false;
        }

    }
}
