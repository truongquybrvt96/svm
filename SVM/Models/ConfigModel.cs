﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SVM.Models
{
    [Table("Configs")]
    public class ConfigModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Required]
        public string SaveFolder { get; set; }
        [Required]
        public int NewChapterCheck { get; set; }
    }
}
