﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SVM.Interfaces
{
    public interface IWebModel
    {
        Uri DomainUri { get; set; }
        string Xpath { get; set; }
    }
}
