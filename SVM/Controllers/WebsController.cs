﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SVM.Entities;
using SVM.Models;
using SVM.Models.InputModels;
using SVM.Models.Resources;
using SVM.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SVM.Controllers
{
    [Route("api/webs")]
    public class WebsController : Controller
    {
        private WebService _webService;
        private SVMContext _context;
        public WebsController()
        {
            _webService = new WebService();
            _context = new SVMContext();
        }
        [HttpGet("GetWebs")]
        public IActionResult Get()
        {
            try
            {
                return Ok(_context.Webs.Select(w => new WebModel {
                    Domain = w.Domain,
                    Id = w.Id,
                    Name = w.Name,
                    Stories = w.Stories.Select(s => new StoryModel {
                        AutoRender = s.AutoRender,
                        Chapters = new List<ChapterModel>(),
                        Id = s.Id,
                        Name = s.Name,
                        Url = s.Url,
                        WebId = s.WebId
                    }).ToList()
                }).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("GetChapters/{storyId}")]
        public IActionResult GetChapters(int storyId)
        {
            try
            {
                return Ok(_context.Chapters.Where(c => c.StoryId == storyId).Select(newChapter => new ChapterModel
                {
                    Id = newChapter.Id,
                    Name = newChapter.Name,
                    NewlyAdded = newChapter.NewlyAdded,
                    StoryId = newChapter.StoryId,
                    Url = newChapter.Url
                }).ToList().OrderByDescending(o => o.Name));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost("CreateStory")]
        public IActionResult CreateStory([FromBody]StoryInputModel storyInputModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(storyInputModel);
            try
            {
                //Get data from story url
                var story = _webService.GetStory(storyInputModel.Url);
                if (story == null)
                    return BadRequest("Cannot get story data.");
                story.WebId = storyInputModel.WebId;


                //Add received story to db
                _context.Stories.Add(story);
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


        [HttpGet("Render/{chapterId}")]
        public IActionResult Render(int chapterId)
        {
            try
            {
                using (var context = new SVMContext())
                {
                    var chapter = context.Chapters.FirstOrDefault(c => c.Id == chapterId);
                    if (chapter == null)
                        return BadRequest(chapterId);

                    var output = FFmpegService.RenderChapter(chapter);
                    return Ok(output);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
            
        }

        [HttpGet("ToggleAutoRender/{storyId}")]
        public IActionResult ToggleAutoRender(string storyId)
        {
            int result;
            try
            {
                result = _webService.ToggleAutoRender(storyId);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }

            return Ok(result);
        }

        [HttpDelete("DeleteStory/{storyId}")]
        public IActionResult DeleteStory(int storyId)
        {
            try
            {
                using (var context = new SVMContext())
                {
                    var story = context.Stories.Include(s => s.Chapters).FirstOrDefault(s => s.Id == storyId);
                    context.Remove(story);
                    return Ok(context.SaveChanges());
                }
            }
            catch (Exception ex)
            {
                return StatusCode(50, ex);
            }

        }

        [HttpGet("Update")]
        public IActionResult Update()
        {
            APIResource resource = new APIResource();
            bool output;
            try
            {
                output = MonitoringService.Update();
            }
            catch (Exception ex)
            {
                resource.Success = false;
                resource.Error = ex;
                resource.DateTime = DateTime.Now;
                return StatusCode(500, resource);
            }
            resource.Data = output;
            resource.Success = true;
            resource.DateTime = DateTime.Now;
            resource.Error = null;
            return Ok(resource);
        }


    }
}
