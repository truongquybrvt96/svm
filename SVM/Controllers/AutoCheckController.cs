﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using SVM.Entities;
using SVM.Interfaces;
using SVM.Models;
using SVM.Services;
using SVM.SignalR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;



namespace SVM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutoCheckController : ControllerBase
    {
        private IHubContext<AutoCheckHub> _hub;

        public AutoCheckController(IHubContext<AutoCheckHub> hub)
        {
            _hub = hub;
        }

        /// <summary>
        /// Auto update gate.
        /// </summary>
        /// <returns></returns>
        public IActionResult Get()
        {
            try
            {
                ConfigModel defaultConfig;
                using (SVMContext context = new SVMContext())
                {
                    defaultConfig = context.Configs.FirstOrDefault(c => c.Name == "default");
                }
                if (defaultConfig == null)
                {
                    defaultConfig = new ConfigModel
                    {
                        NewChapterCheck = 2
                    };
                }

                var timerManager = new TimerManager(() =>
                {
                    //Update chapters in stories
                    bool hasNewUpdate = MonitoringService.Update();
                    _hub.Clients.All.SendAsync("UpdateStory", hasNewUpdate).Wait();

                    //Get newly added chapters
                    var chapters = MonitoringService.GetNewlyAddedChapters();
                    _hub.Clients.All.SendAsync("FindNewChapters", chapters.Count).Wait();

                    //If new chapters found, render.
                    if(chapters.Count > 0)
                         MonitoringService.Render(chapters, (data) => {
                             _hub.Clients.All.SendAsync("RenderProcess", data).Wait();
                         });


                }, defaultConfig.NewChapterCheck * 60 * 1000);

                return Ok(new { Message = "AutoCheckController: Request Completed" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

    }
}
