﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SVM.Entities;
using SVM.Models;
using SVM.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SVM.Controllers
{
    [Route("api/[controller]")]
    public class ConfigController : Controller
    {
        private ConfigService configService;
        public ConfigController()
        {
            configService = ConfigService.GetInstance();
        }
        [HttpGet("Get")]
        public IActionResult Get()
        {
            try
            {
                return Ok(configService.LoadConfig());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost("Save")]
        public IActionResult Save([FromBody]ConfigModel configModel)
        {
            try
            {
                //Check valid
                var DirInfo = Directory.CreateDirectory(configModel.SaveFolder);
                var validNCC = configModel.NewChapterCheck > 0 ? true : false;
                if ((DirInfo.Exists && validNCC) == false)
                {
                    return BadRequest("New chapter check phải lớn hơn hoặc bằng 1 và thư mục phải tồn tại trên máy.");
                }
                //Save and load config
                configService.SaveConfig(configModel);
                var updatedConfig = configService.LoadConfig();
                return Ok(updatedConfig);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

    }
}
