﻿using Microsoft.EntityFrameworkCore.Migrations;
using SVM.Common;
using System.IO;

namespace SVM.Migrations
{
    public partial class SeedingDefaultConfig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"INSERT INTO Configs (Name, SaveFolder, NewChapterCheck) VALUES ('default', '{Constants.DefaultSaveFolder}', 2)");
            migrationBuilder.Sql("INSERT INTO Webs (Name, Domain) VALUES ('lhscan', 'https://lhscan.net/')");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Configs WHERE Name = 'default'");
            migrationBuilder.Sql("DELETE FROM Webs WHERE Name = 'lhscan'");
        }
    }
}
