﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using SVM.Models;

namespace SVM.Entities
{
    public class SVMContext : DbContext
    {
        public DbSet<WebModel> Webs { get; set; }
        public DbSet<ConfigModel> Configs { get; set; }
        public DbSet<StoryModel> Stories { get; set; }
        public DbSet<ChapterModel> Chapters { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionStringBuilder = new SqliteConnectionStringBuilder { DataSource = "svmDb.db" };
            var connectionString = connectionStringBuilder.ToString();
            var connection = new SqliteConnection(connectionString);

            optionsBuilder.UseSqlite(connection);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StoryModel>().HasOne(s => s.Web).WithMany(w => w.Stories).HasForeignKey(s => s.WebId);
            modelBuilder.Entity<ChapterModel>().HasOne(c => c.Story).WithMany(s => s.Chapters).HasForeignKey(c => c.StoryId);
        }
    }
}
