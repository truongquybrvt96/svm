﻿using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SVM.Common;
using SVM.Entities;
using SVM.Interfaces;
using SVM.Models;
using SVM.Models.Resources;
using SVM.SignalR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SVM.Services
{
    /// <summary>
    /// Monitors story
    /// </summary>
    public static class MonitoringService
    {
        private static WebService _webService = new WebService();

        /// <summary>
        /// Update current storie's chapters.
        /// </summary>
        public static bool Update()
        {
            bool hasNewUpdate = false;
            //Get stories from db
            using (SVMContext _context = new SVMContext())
            {
                var stories = _context.Stories.Include(s => s.Chapters).ToList();

                //Update chapters in each story by story URL
                foreach (var story in stories)
                {
                    //Get chapters info
                    HtmlWeb web = new HtmlWeb();
                    var storyDoc = web.Load(story.Url);
                    if (storyDoc != null)
                    {
                        var chapters = _webService.GetChapters(storyDoc);
                        if (chapters == null)
                            return false;
                        foreach (var chapter in chapters)
                        {
                            if (!story.Chapters.Any(c => c.Name == chapter.Name))
                            {
                                story.Chapters.Add(chapter);
                                hasNewUpdate = true;
                            }
                            else
                            {
                                var dupChapters = story.Chapters.Where(c => c.Name == chapter.Name);
                                if (dupChapters != null && dupChapters.Count() > 0)
                                {
                                    foreach (var dupChapter in dupChapters)
                                    {
                                        dupChapter.NewlyAdded = false;
                                    }
                                }  
                            }
                        }
                    }
                }
                _context.SaveChanges();
            }
                
            return hasNewUpdate;
        }

        public static List<ChapterModel> GetNewlyAddedChapters()
        {
            var chapters = new List<ChapterModel>();
            using (SVMContext context = new SVMContext())
            {
                var stories = context.Stories.Include(s => s.Chapters).ToList();
                foreach (var story in stories.Where(s => s.AutoRender == true).ToList())
                {
                    chapters.AddRange(story.Chapters.Where(c => c.NewlyAdded).ToList());
                }
            }
            return chapters;
        }

        public static void Render(List<ChapterModel> chapters, Action<APIResource> action)
        {
            try
            {
                RenderProcessResource renderProcessResource = new RenderProcessResource();
                renderProcessResource.Comings = chapters.ToList();


                if (chapters != null && chapters.Count > 0)
                {
                    foreach (var chapter in chapters)
                    {
                        renderProcessResource.Rendering = chapter;

                        APIResource resource = new APIResource
                        {
                            Data = new RenderProcessResource
                            {
                                Comings = renderProcessResource.Comings.Select(c => new ChapterModel {
                                    Id = c.Id,
                                    Name = c.Name,
                                    Url = c.Url,
                                    StoryId = c.StoryId,
                                    Story = c.Story,
                                    NewlyAdded = c.NewlyAdded
                                }).ToList(),
                                Fails = renderProcessResource.Fails.Select(c => new ChapterModel
                                {
                                    Id = c.Id,
                                    Name = c.Name,
                                    Url = c.Url,
                                    StoryId = c.StoryId,
                                    Story = c.Story,
                                    NewlyAdded = c.NewlyAdded
                                }).ToList(),
                                Rendered = renderProcessResource.Rendered.Select(c => new ChapterModel
                                {
                                    Id = c.Id,
                                    Name = c.Name,
                                    Url = c.Url,
                                    StoryId = c.StoryId,
                                    Story = c.Story,
                                    NewlyAdded = c.NewlyAdded
                                }).ToList(),
                                Rendering = new ChapterModel
                                {
                                    Id = 5,
                                    Name = "ádasd"
                                    //Id = renderProcessResource.Rendering.Id,
                                    //Name = renderProcessResource.Rendering.Name,
                                    //NewlyAdded = renderProcessResource.Rendering.NewlyAdded,
                                    //Story = renderProcessResource.Rendering.Story,
                                    //StoryId = renderProcessResource.Rendering.StoryId,
                                    //Url = renderProcessResource.Rendering.Url
                                }
                            },
                            DateTime = DateTime.Now,
                            Success = true
                        };

                        /*APIResource resource = new APIResource
                        {
                            Data = new RenderProcessResource {
                                Comings = renderProcessResource.Comings.ToList(),
                                Fails = renderProcessResource.Fails.ToList(),
                                Rendered = renderProcessResource.Rendered.ToList(),
                                Rendering = new ChapterModel
                                {
                                    Id = renderProcessResource.Rendering.Id,
                                    Name = renderProcessResource.Rendering.Name,
                                    NewlyAdded = renderProcessResource.Rendering.NewlyAdded,
                                    Story = renderProcessResource.Rendering.Story,
                                    StoryId = renderProcessResource.Rendering.StoryId,
                                    Url = renderProcessResource.Rendering.Url
                                }
                            },
                            DateTime = DateTime.Now,
                            Success = true
                        };*/

                        action(resource);
                        var result = FFmpegService.RenderChapter(chapter);

                        //Add success / fail chapter, remove chapter of comings.
                        if (result)
                            renderProcessResource.Rendered.Add(chapter);
                        else
                            renderProcessResource.Fails.Add(chapter);
                        renderProcessResource.Comings.Remove(chapter);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        
    }
}
