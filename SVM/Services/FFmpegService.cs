﻿using SVM.Common;
using SVM.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVM.Services
{
    public class FFmpegService
    {
        private static FFmpegService _instance;
        public static string ffmpegPath = "";

        private FFmpegService()
        {
        }

        public static FFmpegService GetInstance()
        {
            if (_instance == null)
                _instance = new FFmpegService();
            return _instance;
        }

        //C:\Users\quylt1\Pictures\wallpapers
        public string VStackCmdBuilder(string folder)
        {
            var imageFiles = Directory.GetFiles(folder, "*.*", SearchOption.TopDirectoryOnly).Where(s => s.EndsWith("jpeg", StringComparison.CurrentCultureIgnoreCase)
                 || s.EndsWith("jpg", StringComparison.CurrentCultureIgnoreCase)
                 || s.EndsWith("png", StringComparison.CurrentCultureIgnoreCase)).OrderBy(f => f).ToList();

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(" ");
            foreach (var file in imageFiles)
            {
                stringBuilder.Append($"-i {Path.GetFileName(file)} ");
            }
            stringBuilder.Append($"-filter_complex vstack=inputs={imageFiles.Count()} \"{folder}\\output.png\"");

            return stringBuilder.ToString();
        }

        public string FFmpegExe(string cmd, string renderFolder)
        {
            try
            {
                var process = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = Directory.GetCurrentDirectory() + @"\FFmpeg\ffmpeg.exe",
                        Arguments = cmd,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = false,
                        RedirectStandardError = true,
                        WorkingDirectory = renderFolder
                    },
                    EnableRaisingEvents = true
                };
                StringBuilder outputBuilder = new StringBuilder();
                process.Start();
                string processOutput = null;
                while ((processOutput = process.StandardError.ReadLine()) != null)
                {
                    outputBuilder.Append(processOutput);
                }

                return outputBuilder.ToString();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string ScollingCmdBuilder(string imagePath, int imageCount)
        {
            StringBuilder stringBuilder = new StringBuilder();
            var folder = Path.GetDirectoryName(imagePath);
            stringBuilder.Append($" -loop 1 -t {13*imageCount} -i output.png -filter_complex \"color=white:s=850x600,fps=30[bg];[bg][0]overlay=y=-'t*h*0.01':shortest=1[video]\" -r 30 -pix_fmt yuv420p -c:v libx264 -map [video] output.mp4");
            return stringBuilder.ToString();
        }

        public static bool RenderChapter(ChapterModel chapterModel)
        {
            bool isSuccess = false;
            try
            {
                //Create folder for chapter
                var dirInfo = Directory.CreateDirectory(Constants.DefaultSaveFolder + $@"\{chapterModel.Name}");

                //Create a name file.
                File.WriteAllText(dirInfo.FullName + $@"\name.txt", chapterModel.Name);

                //Download Images
                //WebService webService = new WebService();
                //var parentPageUri = new Uri("https://lhscan.net/" + chapterModel.Url);
                //var imageList = webService.GetImageURLs(parentPageUri);
                //webService.DownloadImagesAsync(imageList, dirInfo.FullName, parentPageUri).Wait();

                //Temp: Download images
                WebService webService = new WebService();
                var parentPageUri = new Uri("https://lhscan.net/" + chapterModel.Url);
                var imageList = new List<string>
                {
                    "https://www.whoa.in/20150417-Whoa/Naruto-Engry-Face-with-Super-Power-HD-Wallpaper.jpg",
                    "https://merrychristmas.wiki/wp-content/uploads/2018/12/christmas-wallpapers12.jpg",
                    "http://1.bp.blogspot.com/-wKEZYgjgpqU/UI2evWfR4JI/AAAAAAAACT0/fciL6AuO8TE/s1600/Dell_Wallpaper_.jpg"
                };
                webService.DownloadImagesAsync(imageList, dirInfo.FullName, parentPageUri).Wait();


                //Render
                var ffmpeg = FFmpegService.GetInstance();

                var cmdvstack = ffmpeg.VStackCmdBuilder(dirInfo.FullName);
                var outputVStack = ffmpeg.FFmpegExe(cmdvstack, dirInfo.FullName);

                var cmd = ffmpeg.ScollingCmdBuilder($@"{dirInfo.FullName}\output.png", imageList.Count);
                var output = ffmpeg.FFmpegExe(cmd, dirInfo.FullName);
                isSuccess = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return isSuccess;
        }

    }
}
