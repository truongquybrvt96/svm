﻿using HtmlAgilityPack;
using SVM.Extensions;
using SVM.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.IO;
using SVM.Models;
using SVM.Common;
using SVM.Entities;

namespace SVM.Services
{
    public class WebService
    {
        private HtmlWeb htmlWeb;
        private SVMContext _context;
        public WebService()
        {
            htmlWeb = new HtmlWeb();
            _context = new SVMContext();
        }

        public List<ChapterModel> GetChapters(HtmlDocument storyDoc)
        {
            var parentNode = storyDoc.DocumentNode.SelectSingleNode(@"//table[@class='table table-hover']//tbody");
            if (parentNode == null)
                return null;
            var trNodes = parentNode.Elements("tr").ToList();
            if (trNodes == null)
                return null;

            List<ChapterModel> chapterModels = new List<ChapterModel>();
            foreach (var trNode in trNodes)
            {
                var hrefNode = trNode.SelectSingleNode("td /a[1]");
                if (hrefNode != null)
                {
                    //Chapter href
                    var hrefValue = hrefNode.GetAttributeValue("href", null);
                    //Chapter name
                    var title = hrefNode.GetAttributeValue("title", null);
                    chapterModels.Add(new ChapterModel
                    {
                        Name = title,
                        Url = hrefValue,
                        NewlyAdded = true
                    });
                }
            }

            return chapterModels;
        }

        public StoryModel GetStory(string storyUrl)
        {
            //Load web document
            var doc = htmlWeb.Load(storyUrl);
            if (doc == null)
                return null;

            //Get story basic info
            var storyNameElement = doc.DocumentNode.SelectSingleNode(@"//ul[@class='manga-info']/h1");
            if (storyNameElement == null)
                return null;

            //Get chapters of this story
            var chapterModels = GetChapters(doc);

            //Return story model object
            StoryModel story = new StoryModel
            {
                AutoRender = false,
                Chapters = chapterModels,
                Name = storyNameElement.InnerText,
                Url = storyUrl
            };

            return story;
        }

        public IList<string> GetImageURLs(Uri chapterUrl)
        {
            var doc = htmlWeb.Load(chapterUrl);
            if (doc == null)
                return null;
            //var imageNodes = doc.DocumentNode.SelectNodes("//div[contains(@class,'chapter-content')]/img[contains(@class,'chapter-img')]");
            var parentNode = doc.DocumentNode.SelectNodes("//div[@class = 'chapter-content']").FirstOrDefault();
            var imageNodes = parentNode.SelectNodes("//img[@class= 'chapter-img']");
            if (imageNodes == null)
                return null;
            IList<string> imageSrcList = new List<string>();
            foreach (var imageNode in imageNodes)
            {
                var imageSrc = imageNode.GetAttributeValue("src", null).RemoveTrashChar();
                imageSrcList.Add(imageSrc);
            }
            return imageSrcList;
        }

        public async Task<int> DownloadImagesAsync(IList<string> imageURLs, string saveFolder, Uri parentPageUri)
        {
            int successCount = 0;
            if (Directory.Exists(saveFolder))
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Referrer = parentPageUri;
                    foreach (var imageURL in imageURLs)
                    {
                        using (var result = await httpClient.GetAsync(imageURL))
                        {
                            if (result.IsSuccessStatusCode)
                            {
                                var bytes = await result.Content.ReadAsByteArrayAsync();
                                File.WriteAllBytes(saveFolder + $@"\image{successCount}.jpeg", bytes);
                                successCount++;
                            }
                        }
                    }
                }
            }
            return successCount;
        }

        public int ToggleAutoRender(string storyId)
        {
            SVMContext context = new SVMContext();
            var story = context.Stories.FirstOrDefault(s => s.Id.ToString() == storyId);
            if (story == null)
                return -1;
            var result = story.AutoRender = !story.AutoRender;
            context.SaveChanges();
            return result? 1 : 0;
        }


    }
}
