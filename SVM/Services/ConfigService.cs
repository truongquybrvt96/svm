﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SVM.Common;
using SVM.Entities;
using SVM.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SVM.Services
{
    public sealed class ConfigService
    {
        private static ConfigService _instance;
        private SVMContext _context;
        private ConfigService() {

            _context = new SVMContext();

            var defaultConfig = _context.Configs.FirstOrDefault(c => c.Name == "default");

            if (defaultConfig == null)
            {
                _context.Configs.Add(new ConfigModel
                {
                    Name = "default",
                    NewChapterCheck = 2,
                    SaveFolder = Constants.DefaultSaveFolder
                });
            }
            _context.SaveChanges();

 

        }

        public static ConfigService GetInstance()
        {
            if (_instance == null)
                _instance = new ConfigService();
            return _instance;
        }

        public ConfigModel LoadConfig()
        {
            var config = _context.Configs.FirstOrDefault(c => c.Name == "default");
            return config;
        }

        public void SaveConfig(ConfigModel configModel)
        {
            var defaultConfig = _context.Configs.FirstOrDefault(c => c.Name == "default");
            if(defaultConfig != null)
            {
                defaultConfig.NewChapterCheck = configModel.NewChapterCheck;
                defaultConfig.SaveFolder = configModel.SaveFolder;

                _context.SaveChanges();
            }
        }
    }
}
