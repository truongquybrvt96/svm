﻿using System;
using System.Threading;

public class TimerManager
{
    private Timer _timer;
    private AutoResetEvent _autoResetEvent;
    private Action _action;

    public DateTime TimerStarted { get; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="action">Tasks to do.</param>
    /// <param name="time">Time in miliseconds</param>
    public TimerManager(Action action, int time = 120000)
    {
        _action = action;
        _autoResetEvent = new AutoResetEvent(false);
        _timer = new Timer(Execute, _autoResetEvent, 1000, time);
        TimerStarted = DateTime.Now;
    }

    public void Execute(object stateInfo)
    {
        _action();

        if ((DateTime.Now - TimerStarted).Seconds > 60)
        {
            _timer.Dispose();
        }
    }
}