﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SVM.Common
{
    public static class Constants
    {
        /// <summary>
        /// Default render 
        /// </summary>
        public static string DefaultSaveFolder = Directory.GetCurrentDirectory() + @"\Storage\Rendering";
    }
}
