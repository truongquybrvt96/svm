﻿using Microsoft.EntityFrameworkCore;
using SVM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SVM.Common
{
    public static class Mapper
    {
        public static List<StoryModel> MapToList(this DbSet<StoryModel> set)
        {
            return set.Select(s => new StoryModel
            {
                AutoRender = s.AutoRender,
                Id = s.Id,
                Name = s.Name,
                Url = s.Url,
                WebId = s.WebId,
                Chapters = s.Chapters.Select(c => new ChapterModel
                {
                    Id = c.Id,
                    Name = c.Name,
                    NewlyAdded = c.NewlyAdded,
                    StoryId = c.StoryId,
                    Url = c.Url
                }).ToList()
            }).ToList();
        }
    }
}
